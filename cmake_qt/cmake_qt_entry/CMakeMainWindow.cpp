#include "CMakeMainWindow.h"
#include "./ui_CMakeMainWindow.h"

#include <QDebug>
#include <QFile>

CMakeMainWindow::CMakeMainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::CMakeMainWindow) {
    ui->setupUi(this);
    //设置无边框
    this->setWindowFlags(Qt::FramelessWindowHint);

    connect(ui->pushButtonClose, SIGNAL(clicked()), this, SLOT(close()));
}
CMakeMainWindow::~CMakeMainWindow() {
    delete ui;
}
int CMakeMainWindow::build() {
    // if (styleSheet.length() == 0)
    //     styleSheet = "color: rgb(230, 230, 230);background-color: rgb(15, 9, 9);";
    this->setStyleSheet(styleSheet);
    return 0;
}
// https://blog.csdn.net/qq_36709282/article/details/119960923
void CMakeMainWindow::mousePressEvent(QMouseEvent* e) {
    if (e->button() == Qt::LeftButton) clickPos = e->pos();

    // qDebug() << clickPos;
}
void CMakeMainWindow::mouseMoveEvent(QMouseEvent* e) {
    if (e->buttons() & Qt::LeftButton //左键点击并且移动
        && e->pos().x() >= 0          //范围在窗口内
        && e->pos().y() >= 0 && e->pos().x() <= geometry().width() &&
        e->pos().y() <= geometry().height()) {
        move(e->pos() + pos() - clickPos); //移动窗口
        qDebug() << clickPos;
    }
}
