#include "CMakeMainWindow.h"

#include <QApplication>
#include <QDebug>
#include <QFile>

int main(int argc, char* argv[]) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication a(argc, argv);
    QFile        file(":/qss/main.qss");
    QString      styleSheet;
    if (file.open(QFile::ReadOnly)) {
        styleSheet = QLatin1String(file.readAll());
        // 没有起作用
        // a.setStyleSheet(w.styleSheet);
        file.close();
    }
    else {
        qDebug() << " load style qss file [:/css/main.qss] error!";
    }
    CMakeMainWindow w;

    w.styleSheet = styleSheet;
    int ec       = w.build();
    w.show();
    return a.exec();
}
