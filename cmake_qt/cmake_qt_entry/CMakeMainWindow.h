#ifndef CMAKEMAINWINDOW_H
#define CMAKEMAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QPoint>

QT_BEGIN_NAMESPACE
namespace Ui {
class CMakeMainWindow;
}
QT_END_NAMESPACE

class CMakeMainWindow : public QMainWindow {
    Q_OBJECT

public:
    CMakeMainWindow(QWidget* parent = nullptr);
    ~CMakeMainWindow();

protected:
    void mousePressEvent(QMouseEvent* e);
    void mouseMoveEvent(QMouseEvent* e);

public:
    int build();

public:
    QString styleSheet;

private:
    int    boundaryWidth;
    QPoint clickPos;

private:
    Ui::CMakeMainWindow* ui;
};
#endif // CMAKEMAINWINDOW_H
