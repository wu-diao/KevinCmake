message("")
message("=====I am in Cmake : cmake_group - START=====")
cmake_minimum_required(VERSION 3.0.0)
project(cmake_group VERSION 0.1.0)

include(CTest)
enable_testing()

# cmake_group_EXPORTS defined In MSVC .vcproj file. 
# Used for dll export in cmake_group.h. only for windows
add_compile_definitions(cmake_group_EXPORTS)

message("CMAKE_BINARY_DIR       = " ${EXECUTABLE_OUTPUT_PATH})
message("LIBRARY_OUTPUT_PATH    = " ${LIBRARY_OUTPUT_PATH})
message("EXECUTABLE_OUTPUT_PATH = " ${EXECUTABLE_OUTPUT_PATH})

#//src目录的文件都加入
aux_source_directory (./ SRC_LIST)

include_directories (./)

#//生成动态库(SHARED)
add_library(cmake_group SHARED ${SRC_LIST})

remove_definitions(cmake_group_EXPORTS) # 去除cmake_group_EXPORTS对后续exe编译的干扰

message("=====I am in Cmake : cmake_group - END=====")
message("")
