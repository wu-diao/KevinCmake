/**
 * @copyright   public
 * @license     MIT
 * @author      kevin
 * @file        MySort.h
 */
#ifndef ktMySort_H
#define ktMySort_H

#include <iostream>

#include "cmake_group.h"
/**
 * @brief Math MySort bgra
 */
class ExportedBycmake_group MySort {

public:
    MySort();
    ~MySort();

public:
    /**
     * @brief calculate
     *
     * @return int
     */
    int calculate();
};
#endif
