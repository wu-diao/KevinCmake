/**
 * @license     MIT
 * @author      Kevin
*/
#ifndef _cmake_group_EXPORT_H_
#define _cmake_group_EXPORT_H_
#ifdef _WIN32
#if defined(cmake_group_EXPORTS)
#  define ExportedBycmake_group __declspec(dllexport)
#else
#  define ExportedBycmake_group __declspec(dllimport)
#endif
#elif __linux__
#  define ExportedBycmake_group
#else
#   error "Unknown compiler"
#endif

#endif // _cmake_group_EXPORT_H_
