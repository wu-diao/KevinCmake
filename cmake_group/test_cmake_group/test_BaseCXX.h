﻿/**
 * @license     MIT
 * @author      Kevin
*/
#ifndef _test_BaseCXX_H_
#define _test_BaseCXX_H_


/**
 * @brief test Color.
*/
class test_BaseCXX
{
public:
	/**
	 * @brief main entry function
	 * @return error count.
	*/
	uint32_t main();

};

#endif
