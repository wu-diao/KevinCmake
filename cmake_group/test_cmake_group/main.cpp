/**
 * @license     MIT
 * @author      Kevin
*/

//std
#include <iostream>

//test
#include "test_BaseCXX.h"

using namespace std;
int main(int, char **)
{
	cout << "Hello, test_cmake_group!\n";
	//define
	uint32_t errCount = 0; //error count

	test_BaseCXX tBaseCXX;

	errCount += tBaseCXX.main();
	cout << "..Finish the test for test_cmake_group." << endl;
	return 0;
}
