#include <iostream>
#include <stdio.h>
#include <stdlib.h>

/*
    知识点：使用 vscode的编译
*/

/*
    程序要求：
*/

int main() {

    //找出数组中最大的元素
    int array[ 5 ] = {300, 350, 200, 400, 250};
    int max        = 0;
    for (int i = 0; i < 5; i++) {
        if (array[ i ] > max) {
            max = array[ i ];
        }
    }
    std::cout << " max is " << max << std::endl;
    system("pause");
    return 0;
}
