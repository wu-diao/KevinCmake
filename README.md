# Cmake 学习

参考文章： https://blog.csdn.net/o_0ava0_o/article/details/124740167
# 1、安装
- 如果安装了Qt5.14.2，就不用安装gcc

地址： `C:\Qt\Qt5.14.2\Tools\mingw730_64\bin`
- 把地址加入到环境变量里面
- 再打开cmd 运行：
```
gcc -v
```
- launch.json配置相对路径：

gdb程序所在路径，前面的路径就是我们配置环境变量的路径
```
"miDebuggerPath": "gdb.exe",	
```

task.json:
```
"command": "g++", //g++命令
```
# 2、项目介绍
## 2.1 hello_world
用VS Code的C++配置进行编译的简单项目

## 2.1 hello_cmake
手动写的一个简单的Cmake项目

## 2.2 cmake_qt
添加一个用QtCreator初始化的Cmake项目

## 2.3 cmake_group
一个EXE调用一个DLL的示例

# 3、坑
- 设定路径后，要重启vscode，才可能生效。
- <> 头文件不能加.h

```
#include <iostream.h> //不过
#include <iostream> //通过
```
还有一种是：gcc没有这个头文件。

# 4、正则

- 搜索图片，添加别名
```
<file>images/(.*)</file>
<file alias ="$1">images/$1</file>
 ```
- 正则替换文件头的注释
```
搜索：
/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*/
替换：
/**
 * @license     MIT
 * @author      Kevin
*/
```
