#include "MySort.h"
#include <iostream>

/*
    知识点：使用 Cmake的编译
*/
int main(int, char**) {
    std::cout << "Hello, cmake!\n";
    MySort sort0;
    int    ec = sort0.calculate();

    std::cout << "sort0.calculate() = " << ec << std::endl;
}
