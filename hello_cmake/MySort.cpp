/**
 * @copyright   public
 * @license     MIT
 * @author      kevin
 * @file        MySort.cpp
 */
#include "MySort.h"
#include <math.h>

#include <algorithm>
#include <vector>

//--------------------------------------------------------------------
MySort::MySort() {
}
//--------------------------------------------------------------------
MySort::~MySort() {
}
//--------------------------------------------------------------------
int MySort::calculate() {
    //找出数组中最大的元素
    int       array[ 5 ] = {300, 350, 200, 400, 250};
    int       max        = 0;
    const int count      = 5;

    // after sort
    std::cout << "before sort" << std::endl;
    for (int i = 0; i < count; i++) {
        std::cout << array[ i ] << " ";
    }
    std::cout << std::endl;
    int t, i, j;
    for (i = 0; i < count; i++) {
        t = 1;
        for (j = count - 1; j > i; j--)
            if (array[ j ] < array[ j - 1 ]) //从后面往前两两判断
            {
                std::swap(array[ j ], array[ j - 1 ]); //交换
                t = 0;
            }
        if (t) break; //如一次循环中没有交换，说明已经排好序了，退出循环
    }

    // after sort
    std::cout << "after sort" << std::endl;
    for (int i = 0; i < count; i++) {
        std::cout << array[ i ] << " ";
    }
    std::cout << std::endl;
    return 0;
}
