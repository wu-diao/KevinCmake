/**
 * @copyright   public
 * @license     MIT
 * @author      kevin
 * @file        MySort.h
 */
#ifndef ktMySort_H
#define ktMySort_H

#include <iostream>

/**
 * @brief Math MySort bgra
 */
class MySort {

public:
    MySort();
    ~MySort();

public:
    /**
     * @brief calculate
     *
     * @return int
     */
    int calculate();
};
#endif
