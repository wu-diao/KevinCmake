echo off
setlocal enabledelayedexpansion

echo -----Format file------
rem set start folder
set DIR="%cd%"
rem set clang-format.exe path
SET CLANG_FORMAT=clang-format.exe

SET /a index=0
for /R %DIR% %%f in (*.h, *.c, *.hpp, *.cpp) do (
    %CLANG_FORMAT% -style=file -i %%f
    set /a index+=1
    Set "NUM=0000!index!"&Set "NUM=!NUM:~-4!"
    echo [!NUM!]Format file: %%f
)
echo.
echo Total format !index! files.
echo.
pause
